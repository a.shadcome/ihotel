import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

import { Role } from './../_models/role';
import { User } from './../_models/user';
import { RoomType } from '../_models/room-type';
import { RoomDefaultName } from '../_models/room-default-name';
import { BedComposition } from '../_models/bed-composition';

const users: User[] = [
    { id: 1, username: 'admin', password: 'admin', firstName: 'Admin', lastName: 'User', role: Role.Admin },
    { id: 2, username: 'user', password: 'user', firstName: 'Normal', lastName: 'User', role: Role.User }
];

const roomTypes: RoomType[] = [
    { id: 1, name: 'اتاق یک تخته', hasBedRoom: true, hasLivingRoom: false, hasBathRoom: false },
    { id: 2, name: 'اتاق دو تخته', hasBedRoom: true, hasLivingRoom: false, hasBathRoom: true },
    { id: 3, name: 'سوئیت', hasBedRoom: true, hasLivingRoom: true, hasBathRoom: true }
];

const roomDefaultNames: RoomDefaultName[] = [
    { id: 1, name: 'سوسنی', roomTypeId: 1 },
    { id: 2, name: 'یاسی', roomTypeId: 2 },
    { id: 3, name: 'نسترنی', roomTypeId: 2 },
    { id: 4, name: 'رو به دریا', roomTypeId: 3 },
    { id: 5, name: 'رو به جنگل', roomTypeId: 3 },
    { id: 6, name: 'رو به جاده', roomTypeId: 3 }
];

const bedCompositions: BedComposition[] = [
    { id: 1, name: 'تخت دو نفره 20*30' },
    { id: 2, name: 'تخت یک نفره' },
    { id: 3, name: 'کاناپه' }
];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;

        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());

        // tslint:disable-next-line: typedef
        function handleRoute() {
            switch (true) {
                case url.endsWith('/users/authenticate') && method === 'POST':
                    return authenticate();
                case url.endsWith('/users') && method === 'GET':
                    return getUsers();
                case url.endsWith('/room-types') && method === 'GET':
                    return getRoomTypes();
                case url.endsWith('/room-default-names') && method === 'POST':
                    return getRoomDefaultNames();
                case url.endsWith('/bed-compositions') && method === 'GET':
                    return getBedCompositions();
                case url.match(/\/users\/\d+$/) && method === 'GET':
                    return getUserById();
                default:
                    return next.handle(request);
            }
        }

        // tslint:disable-next-line: typedef
        function authenticate() {
            const { username, password } = body;
            const user = users.find(x => x.username === username && x.password === password);
            if (!user) { return error('نام کاربری و رمز عبور صحیح نیست'); }
            return ok({
                id: user.id,
                username: user.username,
                firstName: user.firstName,
                lastName: user.lastName,
                role: user.role,
                token: `fake-jwt-token.${user.id}`
            });
        }

        // tslint:disable-next-line: typedef
        function getUsers() {
            if (!isAdmin()) { return unauthorized(); }
            return ok(users);
        }

        // tslint:disable-next-line: typedef
        function getUserById() {
            if (!isLoggedIn()) { return unauthorized(); }

            // only admins can access other user records
            if (!isAdmin() && currentUser().id !== idFromUrl()) { return unauthorized(); }

            const user = users.find(x => x.id === idFromUrl());
            return ok(user);
        }

        function ok(body) {
            return of(new HttpResponse({ status: 200, body }));
        }

        // tslint:disable-next-line: typedef
        function unauthorized() {
            return throwError({ status: 401, error: { message: 'unauthorized' } });
        }

        // tslint:disable-next-line: typedef
        function error(message: string) {
            return throwError({ status: 400, error: { message } });
        }

        // tslint:disable-next-line: typedef
        function isLoggedIn() {
            const authHeader = headers.get('Authorization') || '';
            return authHeader.startsWith('Bearer fake-jwt-token');
        }

        // tslint:disable-next-line: typedef
        function isAdmin() {
            return isLoggedIn() && currentUser().role === Role.Admin;
        }

        // tslint:disable-next-line: typedef
        function currentUser() {
            if (!isLoggedIn()) { return; }
            // tslint:disable-next-line: radix
            const id = parseInt(headers.get('Authorization').split('.')[1]);
            return users.find(x => x.id === id);
        }

        // tslint:disable-next-line: typedef
        function idFromUrl() {
            const urlParts = url.split('/');
            // tslint:disable-next-line: radix
            return parseInt(urlParts[urlParts.length - 1]);
        }

        function getRoomTypes() {
            if (!isAdmin()) { return unauthorized(); }
            return ok(roomTypes);
        }

        function getRoomDefaultNames() {
            if (!isAdmin()) { return unauthorized(); }
            const roomTypeId = body;
            return ok(roomDefaultNames.filter(r => r.roomTypeId === roomTypeId));
        }

        function getBedCompositions() {
            if (!isAdmin()) { return unauthorized(); }
            return ok(bedCompositions);
        }
    }
}

export const fakeBackendProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
