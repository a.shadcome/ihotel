import { Component, OnInit } from '@angular/core';
import { RoomType } from '../_models/room-type';
import { RoomService } from '../_services/room.service';
import { first } from 'rxjs/operators';
import { Room } from '../_models/room';
import { RoomDefaultName } from '../_models/room-default-name';
import { BedComposition } from '../_models/bed-composition';

@Component({
  selector: 'ih-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  roomTypes: RoomType[] = [];
  roomDefaultNames: RoomDefaultName[] = [];
  bedCompositions: BedComposition[] = [];
  selectedRoomType: RoomType = {};
  room: Room = { RoomTypeId: null };
  bedRoomCount = [];
  livingRoomCount = [];

  constructor(private roomSrvc: RoomService) { }

  ngOnInit(): void {
    this.getRoomTypes();
    this.getBedCompositions();
  }

  getRoomTypes(): void {
    this.roomSrvc.getRoomTypes().pipe(first()).subscribe(
      (roomTypes) => {
        this.roomTypes = roomTypes;
      }
    );
  }

  getRoomDefaultNames(roomTypeId: number): void {
    this.roomSrvc.getRoomDefaultNames(roomTypeId).pipe(first()).subscribe(
      (RoomDefaultNames) => {
        this.roomDefaultNames = RoomDefaultNames;
      }
    );
  }

  roomTypeChange(roomTypeId: number): void {
    this.selectedRoomType = this.roomTypes.find(r => r.id === roomTypeId);
    this.getRoomDefaultNames(roomTypeId);
  }

  setBed(): void {
    this.bedRoomCount = [];
    for (let index = 1; index <= this.room.BedRoomQuantity; index++) {
      this.bedRoomCount.push({ name: 'اتاق خواب ' + index, copositions: [{ bedCompositionId: null, count: null }] })
    }
  }

  addCop(copositions: any[]) {
    copositions.push({ bedCompositionId: null, count: null });
  }

  setliving(): void {
    this.livingRoomCount = [];
    for (let index = 1; index <= this.room.LivingRoomQuantity; index++) {
      this.livingRoomCount.push({ name: 'پذیرایی ' + index, copositions: [{ bedCompositionId: null, count: null }] })
    }
  }

  getBedCompositions() {
    this.roomSrvc.getBedCompositions().pipe(first()).subscribe(
      (bedCompositions) => {
        this.bedCompositions = bedCompositions;
      }
    );
  }

  submit(): void {
    this.room.RoomWards = [];
    for (const iterator of this.livingRoomCount) {
      this.room.RoomWards.push(iterator);
    }
    for (const iterator of this.bedRoomCount) {
      this.room.RoomWards.push(iterator);
    }
    console.log(this.room);
  }

}
