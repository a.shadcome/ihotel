export class Room {
    RoomTypeId?: number;
    RoomDefaultNameId?: number;
    CustomName?: string;
    BedRoomQuantity?: number;
    LivingRoomQuantity?: number;
    BathRoomQuantity?: number;
    RoomWards?: any[]
}
