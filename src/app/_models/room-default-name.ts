export class RoomDefaultName {
    id: number;
    name: string;
    roomTypeId: number;
}
