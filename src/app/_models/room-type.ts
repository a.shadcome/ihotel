export class RoomType {
    id?: number;
    name?: string;
    hasBedRoom?: boolean;
    hasLivingRoom?: boolean;
    hasBathRoom?: boolean;
}
