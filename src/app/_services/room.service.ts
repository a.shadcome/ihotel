import { Injectable } from '@angular/core';
import { RoomType } from '../_models/room-type';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { RoomDefaultName } from '../_models/room-default-name';
import { BedComposition } from '../_models/bed-composition';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  constructor(private http: HttpClient) { }

  getRoomTypes() {
    return this.http.get<RoomType[]>(`${environment.apiUrl}/room-types`);
  }

  getRoomDefaultNames(roomTypeId: number) {
    return this.http.post<RoomDefaultName[]>(`${environment.apiUrl}/room-default-names`, roomTypeId);
  }

  getBedCompositions() {
    return this.http.get<BedComposition[]>(`${environment.apiUrl}/bed-compositions`);
  }
}
